export const fetchStudentsBegin = () => ({
    type: "FETCH_STUDENTS_BEGIN"
});
export const createStudentsBegin = () => ({
    type: "CREATE_STUDENTS_BEGIN"
});
export const createStudentsSuccess = (student) => ({
    type: "CREATE_STUDENTS_SUCCESS",
    payload:student
});
export const deleteStudentsBegin = () => ({
    type: "DELETE_STUDENTS_BEGIN"
});
export const deleteStudentsSuccess = (_id) => ({
    type: "DELETE_STUDENTS_SUCCESS",
    payload:_id
});

export const fetchStudentsSuccess = students => ({
    type: "FETCH_STUDENTS_SUCCESS",
    payload: students
});

export const fetchError = error => ({
    type: "FETCH_ERROR",
    payload: error
});