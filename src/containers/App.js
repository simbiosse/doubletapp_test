import React, {Component} from 'react';
import {connect} from 'react-redux'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import NewStudentModal from "../components/newStudentModal"
import StudentRow from "../components/studentRow"
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Paper from '@material-ui/core/Paper';
import LinearProgress from '@material-ui/core/LinearProgress';
import Divider from '@material-ui/core/Divider';

import 'whatwg-fetch'
import {
    fetchError,
    fetchStudentsBegin,
    fetchStudentsSuccess,
    deleteStudentsBegin,
    deleteStudentsSuccess,
    createStudentsBegin,
    createStudentsSuccess
} from "../actions/actions"

const styles = {
    paper: {
        width: "100%",
    },
    text: {
        color: "rgba(0, 0, 0, 0.87)",
        fontSize: "1rem",
        fontWeight: 400,
        fontFamily: " 'Roboto', 'Helvetica', 'Arial', sans-serif",
        lineHeight: "1.5em"
    },
    toolbar: {
        justifyContent: "center"
    },
    addButton: {
        position: "fixed",
        bottom: "2%",
        right: "2%"
    },

    loadElement: {}
}


const mapDispatchToProps = (dispatch) => {
    return {
        getStudents: () => {
            dispatch(fetchStudentsBegin())
            return fetch("/api/students")
                .then(res => res.json())
                .then(res => dispatch(fetchStudentsSuccess(res)))
                .catch(error => dispatch(fetchError(error)));
        },
        deleteStudent: (id) => {
            const headers = new Headers();
            headers.append("Content-Type", "application/json");
            let body = JSON.stringify({id})
            dispatch(deleteStudentsBegin())
            return fetch("/api/students", {method: "delete", body: body, headers})
                .then(res => res.json())
                .then(res => dispatch(deleteStudentsSuccess(res)))
                .catch(error => dispatch(fetchError(error)))
        },
        createStudent: (student) => {
            createStudentsBegin()
            const headers = new Headers();
            headers.append("Content-Type", "application/json");

            let body = JSON.stringify(student)
            return fetch("/api/students", {method: "post", body: body, headers})
                .then(res => res.json())
                .then(res => {
                    console.log(res)
                    return res
                })
                .then(res => dispatch(createStudentsSuccess(res)))
                .catch(error => dispatch(fetchError(error)));
        },
        openDialog: () => dispatch({type: "OPEN_DIALOG"}),
        closeDialog: () => dispatch({type: "CLOSE_DIALOG"})
    }
}
const mapStateToProps = (state) => {
    return {
        loading: state.loading,
        dialogIsOpen: state.dialogIsOpen,
        students: state.students
    }
}

class App extends Component {
    componentWillMount() {
        this.props.getStudents()
    }

    render() {
        return (
            <div style={{flexGrow: "1"}}>

                <AppBar position="static" color="primary">
                    <Toolbar style={styles.toolbar}>
                        <Typography variant="title" color="inherit">
                            DOUBLETAPP
                        </Typography>
                    </Toolbar>
                </AppBar>
                {this.props.loading ?
                    <LinearProgress color={"secondary"}/>
                    : null
                }
                {this.props.students.length < 1 ?
                    null :
                    <Paper style={styles.paper}>
                        <List style={{paddingBottom: 0}}>
                            <div>
                                <ListItem>
                                    <ListItemText style={{flex: "1 1"}} primary="Фамилия"/>
                                    <ListItemText style={{flex: "1 1"}} primary="Имя"/>
                                    <ListItemText style={{flex: "1 1"}} primary="Рейтинг"/>
                                    <ListItemIcon>
                                        <span style={styles.text}>Удалить </span>
                                    </ListItemIcon>
                                </ListItem>
                            </div>
                            <Divider/>
                            {this.props.students.map(student => <StudentRow deleteStudent={this.props.deleteStudent}
                                                                            student={student}
                                                                            key={student._id}
                            />)}
                        </List>
                    </Paper>}


                <Button onClick={() => this.props.openDialog()} variant="fab" color="primary" aria-label="add"
                        style={styles.addButton}>
                    <AddIcon/>
                </Button>
                <NewStudentModal open={this.props.dialogIsOpen}
                                 closeDialog={this.props.closeDialog}
                                 sumbitAction={this.props.createStudent}
                />
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
