import React from 'react';
import ReactDOM from 'react-dom';
import App from "./containers/App.js";
import {createStore,applyMiddleware} from "redux"
import ReduxThunk from 'redux-thunk'
import {Provider} from "react-redux"
import rootReducer from "./reducers/rootReducer"
import Logger from "redux-logger"


const store = createStore( rootReducer,applyMiddleware(ReduxThunk,Logger))


ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

