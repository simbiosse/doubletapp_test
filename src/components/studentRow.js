import React from "react"

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import IconButton from "@material-ui/core/IconButton"
import Divider from '@material-ui/core/Divider';

export default class StudentRow extends React.Component {
    state = {
        dialogOpen: false
    }

    render() {
        const student = this.props.student
        return (
            <div>
                <ListItem>

                    <ListItemText style={{flex: "1 1"}} primary={student.lastName}/>
                    <ListItemText style={{flex: "1 1"}} primary={student.firstName}/>
                    <ListItemText style={{flex: "1 1"}} primary={student.points}/>
                    <ListItemIcon>
                        <IconButton onClick={() => this.setState({dialogOpen: true})}>
                            <DeleteIcon/>
                        </IconButton>
                    </ListItemIcon>
                </ListItem>
                <Divider/>
                <Dialog
                    open={this.state.dialogOpen}
                    onClose={() => this.setState({dialogOpen: false})}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogContent>
                        <DialogContentText>
                            Вы действительно хотите удалить этого студента из списка?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({dialogOpen: false})} color="primary">
                            Отменить
                        </Button>
                        <Button onClick={() => {
                            this.setState({dialogOpen: false})
                            this.props.deleteStudent(student._id)
                        }} color="primary">
                            Подтвердить
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}