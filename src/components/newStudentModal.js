import React from "react"
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

export default class NewStudentModal extends React.Component {
    state = {
        firstName: "",
        lastName: "",
        points: ""
    }

    render() {
        const resetInputs = () => this.setState({firstName: "", lastName: "", points: ""})
        const closeDialog = () => {
            resetInputs()
            this.props.closeDialog()
        }
        //Валидация рейтинга
        const handlePoints = (val) => {
            let _val = parseInt(val)
            if (Number.isNaN(_val) || _val < 1) {
                return this.setState({points: ""})
            }
            this.setState({points: _val})
        }

        return (
            <Dialog
                open={this.props.open}
                onClose={() => closeDialog()}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Добавить студента</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Введите имя, фамилию и рейтинг студента
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="first_name"
                        label="Имя"
                        type="text"
                        fullWidth
                        value={this.state.firstName}
                        onChange={(e) => this.setState({firstName: e.target.value})}
                    />
                    <TextField
                        margin="dense"
                        id="last_name"
                        label="Фамилия"
                        type="text"
                        fullWidth
                        value={this.state.lastName}
                        onChange={(e) => this.setState({lastName: e.target.value})}
                    />
                    <TextField
                        margin="dense"
                        id="rating"
                        label="Рейтинг"
                        type="text"
                        fullWidth
                        value={this.state.points}
                        onChange={e => handlePoints(e.target.value)}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => closeDialog()} color="primary">
                        Отменить
                    </Button>
                    <Button
                        onClick={() => {
                            this.props.sumbitAction({
                                firstName: this.state.firstName,
                                lastName: this.state.lastName,
                                points: this.state.points
                            })
                            closeDialog()
                        }}
                        color="primary"
                        disabled={!this.state.firstName || !this.state.lastName || !this.state.points}>
                        Добавить
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}