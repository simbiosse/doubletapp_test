const initialState = {
    dialogIsOpen: false,
    loading: false,
    students: []
}

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_STUDENTS_BEGIN": {
            return {...state, loading: true}
        }
        case "FETCH_STUDENTS_SUCCESS": {
            const students = action.payload
            return {...state, loading: false, students}
        }
        case "CREATE_STUDENTS_BEGIN": {
            return {...state, loading: true}
        }
        case "CREATE_STUDENTS_SUCCESS": {
            const student = action.payload
            const students = [...state.students, student]
            return {...state, students, loading: false}
        }
        case "DELETE_STUDENTS_BEGIN": {
            return {...state, loading: true}
        }
        case "DELETE_STUDENTS_SUCCESS": {
            const id = action.payload.id
            let students = [...state.students]
            students = students.filter(el=>el._id!==id)
            return {...state, students, loading: false}
        }


        case "OPEN_DIALOG": {
            const dialogIsOpen = true;
            return {...state, dialogIsOpen}

        }
        case "CLOSE_DIALOG": {
            const dialogIsOpen = false;
            return {...state, dialogIsOpen}

        }
        default: {
            return state
        }
    }
}

export default rootReducer