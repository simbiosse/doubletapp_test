const express = require('express');
const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient;
const bodyParser = require('body-parser');
const app = express();
const port = 8000;
const MONGO_URL = "mongodb://root:root@ds131800.mlab.com:31800/doubletapp";
const path = require("path")
const dir = path.resolve(__dirname)


MongoClient.connect(MONGO_URL, (err, client) => {
    if (err) return console.log(err)

    const collection = client.db("doubletapp").collection("students");

    app.use(bodyParser.json());
    app.use(express.static(dir + "/build"));
    app.get("/", (req, res) => {
        return res.sendFile(dir + "/build/index.html")
    })


    app.get("/api/students", (req, res) => {
        collection.find().toArray((err, docs) => {
            if (err) {
                res.sendStatus(404)
                return res.send(err)
            }

            res.setHeader('Content-Type', 'application/json');
            res.setHeader("Access-Control-Allow-Origin", "*")
            res.send(JSON.stringify(docs))

        })


    })
    app.post("/api/students", (req, res) => {
        console.log(req.body)
        collection.insertOne(req.body, (err, dbRes) => {
            if (err) {
                res.sendStatus(404)
                return res.send(err)
            }

            res.setHeader('Content-Type', 'application/json');
            res.setHeader("Access-Control-Allow-Origin", "*");
            return res.send(JSON.stringify(dbRes.ops[0]))
        })
    })
    app.delete("/api/students", (req, res) => {
        const id = req.body.id
        collection.deleteOne({_id: new mongodb.ObjectID(id)}, (err, dbRes) => {
            if (err) {
                res.sendStatus(404)
                return res.send(err)
            }
            res.setHeader('Content-Type', 'application/json');
            res.setHeader("Access-Control-Allow-Origin", "*")

            return res.send(JSON.stringify({id}))
        })
    })


    app.listen(port, () => {
        console.log('Слушаю ' + port);
    });

})


